# Scripts

This repository houses a bunch of useful scripts for various tasks.

* anews: read arch news
* cmusstat: print what cmus is doing
* custommpv: limit youtube video quality to 1080p
* jbacklight: change screen backlight on a macbookpro 8,3 (requires modified sudoers file)
* kbacklight: change keyboard backlight on a macbookpro 8,3 (requires modified sudoers file)
* setspeed: change fanspeed on a macbookpro 8,3 (requires modified sudoers file)
* mpvit: searches for links that look like youtube links in the clipboard and plays them
* proftime: Shows remaining time per slide for a lecture
* sshot: My private "pastebin" like system. Makes a screenshot and uploads it to my vps.
* touchpadtoggle: Toggle a touchpad on/off

